package simpleRest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleRest.entity.UserEntity;
import simpleRest.mapper.UserMapper;
import simpleRest.service.UserService;

@Service("userEntityService")
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public UserEntity save(UserEntity entity) {
		userMapper.save(entity);
		return entity;
	}

	@Override
	public UserEntity findById(Long id) {
		return userMapper.getById(id);
	}

	@Override
	public UserEntity update(UserEntity entity) {
		userMapper.update(entity);
		return entity;
	}

	@Override
	public void deleteById(Long id) {
		userMapper.deleteById(id);
	}

}
