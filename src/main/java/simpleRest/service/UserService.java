package simpleRest.service;

import simpleRest.entity.UserEntity;

public interface UserService {
	
	UserEntity save(UserEntity entity);
	UserEntity findById(Long id);
	UserEntity update(UserEntity entity);
	void deleteById(Long id);

}
