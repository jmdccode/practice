package simpleRest.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspectConfig {
	
	private static final Logger LOGGER = LogManager.getLogger(LoggingAspectConfig.class);
	private static final String LOGGER_ALL_METHODS = "execution(* simpleRest..*(..)))";
	
	@Before(LOGGER_ALL_METHODS)
	public void logBeforeAllMethods(JoinPoint jointPoint) throws Throwable {
		MethodSignature methodSignature = (MethodSignature) jointPoint.getSignature();
		
		System.out.println(methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName() + "() - START");
		LOGGER.debug(methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName() + "() - START");
	}

	@After(LOGGER_ALL_METHODS)
	public void logAfterAllMethods(JoinPoint joinPoint) throws Throwable {
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		
		System.out.println(methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName() + "() - END");
		LOGGER.debug(methodSignature.getDeclaringType().getSimpleName() + "." + methodSignature.getName() + "() - END");
	}

}
