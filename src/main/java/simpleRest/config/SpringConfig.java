package simpleRest.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootConfiguration
@EntityScan({
	"simpleRest.entity"
})
@ComponentScan(basePackages = { 
	"simpleRest.mapper",
	"simpleRest.service",
	"simpleRest.controller",
	"simpleRest.config"
})
public class SpringConfig {

}
