package simpleRest.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import simpleRest.entity.UserEntity;

@Mapper
public interface UserMapper {
	
	@Insert("INSERT INTO user (name) "
			+ "VALUES (#{name})")
	@Options(useGeneratedKeys=true, keyProperty = "id", keyColumn = "id")
	int save(UserEntity entity);
	
	@Select("SELECT * FROM user WHERE ID = #{id}")
	UserEntity getById(@Param("id") Long id);
	
	@Update("UPDATE user "
			+ "SET name = #{name} "
			+ "WHERE id = #{id}")
	int update(UserEntity entity);
	
	@Delete("DELETE FROM user WHERE id =#{id}")
	void deleteById(@Param("id") Long id);

}
