package simpleRest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface BaseController<T> {

	public ResponseEntity<T> save(@RequestBody T entity);
	public ResponseEntity<T> findById(@RequestParam Long id) throws Throwable;
	public ResponseEntity<T> update(@RequestBody T entity);
	public void deleteById(@RequestParam Long id);

}
