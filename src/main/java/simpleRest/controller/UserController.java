package simpleRest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import simpleRest.entity.UserEntity;
import simpleRest.service.UserService;

@RestController
public class UserController implements BaseController<UserEntity> {
	
	@Autowired
	private UserService userEntityService;

	@PutMapping("/user/save")
	public ResponseEntity<UserEntity> save(UserEntity entity) {
		return ResponseEntity.ok(userEntityService.save(entity));
	}

	@GetMapping("/user/find")
	public ResponseEntity<UserEntity> findById(Long id) throws Throwable {
		return ResponseEntity.ok(userEntityService.findById(id));
	}

	@PatchMapping("/user/update")
	public ResponseEntity<UserEntity> update(UserEntity entity) {
		return ResponseEntity.ok(userEntityService.update(entity));
	}

	@DeleteMapping("/user/delete")
	public void deleteById(Long id) {
		userEntityService.deleteById(id);
	}

}
